#include "detector.hh"

MySensitiveDetector::MySensitiveDetector(G4String name) :
  G4VSensitiveDetector(name)
{}

MySensitiveDetector::~MySensitiveDetector()
{}

G4bool MySensitiveDetector::ProcessHits(G4Step *aStep,
    G4TouchableHistory *ROhist)
{
  G4Track *track = aStep->GetTrack();

  track->SetTrackStatus(fStopAndKill);

  G4StepPoint *preStepPoint = aStep->GetPreStepPoint();
  G4StepPoint *postStepPoint = aStep->GetPostStepPoint();

  G4ThreeVector posPhoton = preStepPoint->GetPosition();

  const G4VTouchable *touchable = aStep->GetPreStepPoint()->GetTouchable();

  G4VPhysicalVolume *physVol = touchable->GetVolume();
  G4ThreeVector posDetector = physVol->GetTranslation();

  G4int copyNo = touchable->GetCopyNumber();
  //G4double energy = aStep->GetDeltaEnergy(); 
  //G4double energy = preStepPoint->GetTotalEnergy();
  G4ThreeVector momPhoton = preStepPoint->GetMomentum();
  G4double wlen = ((1.239841939*eV)/momPhoton.mag())*1e+03;

  //G4cout << copyNo << " Photon position: " <<  posPhoton << G4endl;
  G4cout << copyNo << " wlen=" << wlen << G4endl;
  //G4cout << "Detector position: " << posDetector << G4endl;
  //G4cout << "Copy number: " << copyNo << G4endl;

  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

  /*
  G4AnalysisManager *man = G4AnalysisManager::Instance();
  G4double radius = std::sqrt(posDetector[0]*posDetector[0]
      + posDetector[1]*posDetector[1]);
  
  man->FillNtupleIColumn(0, evt);
  man->FillNtupleDColumn(1, posDetector[0]);
  man->FillNtupleDColumn(2, posDetector[1]);
  man->FillNtupleDColumn(3, radius);
  man->FillNtupleDColumn(4, posDetector[2]);
  man->AddNtupleRow(0);
  */

  return true;
}
