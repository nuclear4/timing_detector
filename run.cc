#include "run.hh"

MyRunAction::MyRunAction()
{
/*
  auto man = G4AnalysisManager::Instance();
  
  man->CreateNtuple("Energy", "Energy");
  man->CreateNtupleIColumn("fEvent");
  man->CreateNtupleDColumn("fX");
  man->CreateNtupleDColumn("fY");
  man->CreateNtupleDColumn("fZ");
  man->CreateNtupleDColumn("fMomMag");
  man->CreateNtupleDColumn("fEnergy");
  man->FinishNtuple(0);

  man->CreateNtuple("TEnergy", "Total Energy");
  man->CreateNtupleDColumn("fEdep");
  man->FinishNtuple(1);
*/
}

MyRunAction::~MyRunAction()
{}

void MyRunAction::BeginOfRunAction(const G4Run* run)
{
/*
  auto man = G4AnalysisManager::Instance();

  G4int runID = run->GetRunID();

  std::stringstream strRunID;
  strRunID << runID;

  man->SetVerboseLevel(1);
  man->SetNtupleMerging(true);

  man->OpenFile("output"+strRunID.str()+".root");


  G4cout << "=================> Starting run " << strRunID.str() << G4endl;
  G4cout << "<=================" << G4endl;
*/
}

void MyRunAction::EndOfRunAction(const G4Run*)
{
/*
  auto man = G4AnalysisManager::Instance();

  man->Write();
  man->CloseFile();
*/
}
