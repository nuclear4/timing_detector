{
  TFile *input = new TFile("output0.root", "read");

  TTree *tree = (TTree*)input->Get("Energy");

  double fEnergy, fMomMag, fX, fY, fZ;
  int fEvent;

  tree->SetBranchAddress("fEnergy", &fEnergy);
  tree->SetBranchAddress("fMomMag", &fMomMag);
  tree->SetBranchAddress("fX", &fX);
  tree->SetBranchAddress("fY", &fY);
  tree->SetBranchAddress("fZ", &fZ);
  tree->SetBranchAddress("fEvent", &fEvent);

  int entries = tree->GetEntries();

  cout << entries << endl;

  for(int i = 0; i < entries; i++)
  { 
    tree->GetEntry(i);

    if (i%1000000 == 0)
    {
      cout << i << endl;
      cout << fX << " " << fY << " " << fZ << ": " << fEnergy << endl;
    }

//    if (fX > 0. && fY > 0. && fZ > 0. && fEnergy > 0.)
  }

  input->Close();

}
