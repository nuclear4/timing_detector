#include "construction.hh"

MyDetectorConstruction::MyDetectorConstruction()
{
  DefineMaterials();
}

MyDetectorConstruction::~MyDetectorConstruction()
{}

void MyDetectorConstruction::DefineMaterials()
{
  G4NistManager *nist = G4NistManager::Instance();
  

  // scintillator
  scinMat = nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
 
  G4MaterialPropertiesTable *scintillatorProperties = new G4MaterialPropertiesTable();

  scintillatorProperties->AddConstProperty("SCINTILLATIONYIELD", 9./MeV);
  //scintillatorProperties->AddConstProperty("FASTTIMECONSTANT", 0.7*ns);
  // FASTTIMECONSTANT -> SCINTILLATIONTIMECONSTANT1
  scintillatorProperties->AddConstProperty("SCINTILLATIONTIMECONSTANT1", 0.7*ns);
  scintillatorProperties->AddConstProperty("RESOLUTIONSCALE", 1.0);
  //scintillatorProperties->AddConstProperty("YIELDRATIO", 1.0);
  // YIELDRATIO -> SCINTILLATIONYIELD1
  scintillatorProperties->AddConstProperty("SCINTILLATIONYIELD1", 1.0);

  const G4int NPHOTONENERGIES = 22;
  G4double scintResponseWavelengths[NPHOTONENERGIES] = {
    3.26*eV, 3.22*eV, 3.17*eV, 3.13*eV, 3.10*eV, 3.06*eV, 3.02*eV, 
    2.98*eV, 2.95*eV, 2.91*eV, 2.88*eV, 2.83*eV, 2.78*eV, 2.75*eV, 
    2.72*eV, 2.69*eV, 2.66*eV, 2.63*eV, 2.61*eV, 2.58*eV, 2.53*eV, 2.50*eV
  };
  G4double scintResponseSpectrum[NPHOTONENERGIES] = {
    1.69, 3.13, 4.21, 4.92, 7.8, 9.59, 11.75, 15.16, 18.93, 
    25.22, 35.11, 43.9, 48.57, 53.6, 60.78, 85.93, 99.4, 
    91.32, 75.69, 35.28, 17.5, 4.38
  };
  G4double scintRefractiveIndex[NPHOTONENERGIES];
  G4double scintAbsLength[NPHOTONENERGIES];
  for (int i=0; i<NPHOTONENERGIES; i++) {
    scintRefractiveIndex[i] = 1.58;
    scintAbsLength[i] = 140.*cm;
  }
  //scintillatorProperties->AddProperty("FASTCOMPONENT", scintResponseWavelengths, scintResponseSpectrum, NPHOTONENERGIES);
  // FASTCOMPONENT -> SCINTILLATIONCOMPONENT1
  scintillatorProperties->AddProperty("SCINTILLATIONCOMPONENT1", scintResponseWavelengths, scintResponseSpectrum, NPHOTONENERGIES);
  scintillatorProperties->AddProperty("RINDEX", scintResponseWavelengths, scintRefractiveIndex, NPHOTONENERGIES);
  scintillatorProperties->AddProperty("ABSLENGTH", scintResponseWavelengths, scintAbsLength, NPHOTONENERGIES);

  scinMat->SetMaterialPropertiesTable(scintillatorProperties);

  //_________________________________________________________________
  
  // world
  worldMat = nist->FindOrBuildMaterial("G4_AIR");

  G4double energy[2] = {1.239841939*eV/0.9, 1.239841939*eV/0.2};
  G4double rindexWorld[2] = {1.0, 1.0};
  G4MaterialPropertiesTable *mptWorld = new G4MaterialPropertiesTable();
  mptWorld->AddProperty("RINDEX", energy, rindexWorld, 2);
  worldMat->SetMaterialPropertiesTable(mptWorld);
}

G4VPhysicalVolume *MyDetectorConstruction::Construct()
{
  // Size
  G4double xWorld = 30*cm;
  G4double yWorld = 20*cm;
  G4double zWorld = 20*cm;
  
  G4double xScin = 20*cm;
  G4double yScin = 3*6*mm;
  G4double zScin = 6*mm;
  
  //_________________________________________________________________

  solidWorld = new G4Box("solidWorld", xWorld, yWorld, zWorld);

  logicWorld = new G4LogicalVolume(solidWorld, worldMat,
      "logicWorld");

  physWorld = new G4PVPlacement(0,
      G4ThreeVector(0., 0., 0.), logicWorld, "physWorld", 0, false, 0, true);

  // 6 mm is max SiPM size
  // 3, 4, 6 mm variants are also available
  solidRadiator = new G4Box("solidRadiator", xScin, yScin, zScin);

  logicRadiator = new G4LogicalVolume(solidRadiator,
      scinMat, "logicalRadiator");

  //_________________________________________________________________
  // Detector
  
  solidDetector = new G4Box("solidDetector", 0.2*mm, yScin, zScin);
  logicDetector = new G4LogicalVolume(solidDetector, scinMat, "logicDetector");
  // bake detectors into scintillator
  physDetector = new G4PVPlacement(0, G4ThreeVector(xScin, 0., 0.),
      logicDetector, "physDetector", logicRadiator, false, 0, true);
  physDetector = new G4PVPlacement(0, G4ThreeVector(-xScin, 0., 0.),
      logicDetector, "physDetector", logicRadiator, false, 1, true);
  
  //_________________________________________________________________

  fScoringVolume = logicRadiator;

  physRadiator = new G4PVPlacement(0,
      G4ThreeVector(0., 0., 0.), logicRadiator, "physRadiator",
      logicWorld, false, 0, true);

  return physWorld;
}

void MyDetectorConstruction::ConstructSDandField()
{ 
  MySensitiveDetector *sensDet = new MySensitiveDetector("SensitiveDetector");

  logicDetector->SetSensitiveDetector(sensDet);
}

