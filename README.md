# Timing Resolution for Scintillating Detectors

Simulations for studying which detector geometry gives best timing resolution

## Usage

After setting up Geant4, do:

```bash
mkdir build && cd build
cmake ..
make -j4
./sim
```

For running simulation several times, use a macro: `./sim run.mac`.
Data will be saved to a root file which can then be analyzed using root:
```bash
root output0.root
```

In root, you can for example use `TBrowser` to quickly study histograms:
```c++
new TBrowser
```

Plot correlation histograms for energy penetration:
```c++
Energy->Draw("fEnergy:fZ", "", "colz")
```

Zoom into proton beam:
```c++
Energy->Draw("fEnergy:fZ", "fEnergy > 30", "colz")
```

## Compatability

This has been tested to work with geant4-v11.0.2 on Linux and 8-core 8GB machine.

## Resources

## Acknowledgements
* Geant4 Project Template from https://github.com/MustafaSchmidt/geant4-tutorial
